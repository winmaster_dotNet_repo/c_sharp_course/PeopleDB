﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace KatarzynaBialasLab2
{
    public class SaveAndRead
    {
        public SaveAndRead()
        {
            
        }

        public void SaveDataSerializationPerson(List<Person> list, string name)
        {
            string file = name;
            //serialize
            using (Stream stream = File.Open(name, FileMode.Create))
            {
                var binarryformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binarryformatter.Serialize(stream, list);
            }
        }

        public List<Person> ReadDataDeserializationPerson(string name)
        {
            string file = name;
            List<Person> list;

            //deserialize
            using (Stream stream = File.Open(name, FileMode.Open))
            {
                var binarryformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                list = (List<Person>)binarryformatter.Deserialize(stream);
            }
            return list;
        }
    }
}
