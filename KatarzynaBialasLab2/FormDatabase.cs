﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasLab2
{
    public partial class Lab2 : Form
    {
        #region inicjowanie zmiennych globalnych
        List<Person> listOfPeople=new List<Person>();
        FormAddGrade formAddGrade;
        FormAddPerson formAddPerson;
        #endregion

        #region  Funkcje sluzace do przekazywania pol formularzy

        private string GetFormAddPersonTextBoxName()
        {
            return formAddPerson.TextBoxName.Text;
        }

        private string GetFormAddPersonTextBoxSurname()
        {
            return formAddPerson.TextBoxSurname.Text;
        }

        private string GetFormAddPersonTextBoxAge()
        {
            return formAddPerson.TextBoxAge.Text;
        }

        private string GetFormAddGradeTextBoxGrade()
        {
            return formAddGrade.TextBoxGrade.Text;
        }

        #endregion

        #region inicjalizacja komponentu
        public Lab2()
        {
            InitializeComponent();
            Person person = new Person("", "",0);

            //cwiczenia obiektowosc
            person.Name = "Jan";
            person.Surname = "Kowalski";
            person.Age = 25;
            Superman superman = new Superman(person.Name, person.Surname, person.Age, 15);
            listOfPeople.Add(person);
            listOfPeople.Add(new Person("Teresa", "Mendel", 20));
            listOfPeople.Add(new Person("Janusz", "Andrzej", 15));
            listOfPeople.Add(new Person("Janusz", "Ratek", 99));
            listOfPeople.Add(new Person("Edward", "Bielik", 88));
            listOfPeople.Add(new Person("Zbigniew", "Hendrykowski", 75));
        }
        #endregion

        #region opracowanie funkcji przyciskow w aplikacji

        private void buttonShow_Click(object sender, EventArgs e)
        {
            //przypisanie nulla umozliwa refresh
            dataGridViewListOfPeople.DataSource = null;
            dataGridViewListOfPeople.DataSource=listOfPeople;
            dataGridViewListOfPeople.BackgroundColor = Color.Aqua;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            SaveAndRead sv = new SaveAndRead();
            sv.SaveDataSerializationPerson(listOfPeople, "ListOfPeopleDatabase.bin");
            MessageBox.Show("Pomyślnie zapisano dane do pliku");
        }

        private void buttonReadFile_Click(object sender, EventArgs e)
        {
            SaveAndRead sv = new SaveAndRead();
            dataGridViewListOfPeople.DataSource = null;
            dataGridViewGrades.DataSource = null;
            dataGridViewListOfPeople.DataSource= sv.ReadDataDeserializationPerson("ListOfPeopleDatabase.bin");
            listOfPeople = null;
            listOfPeople = sv.ReadDataDeserializationPerson("ListOfPeopleDatabase.bin");
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            formAddPerson = new FormAddPerson();
            formAddPerson.ShowDialog();
            string name = GetFormAddPersonTextBoxName();
            string surname = GetFormAddPersonTextBoxSurname();
            int age = Int32.Parse(GetFormAddPersonTextBoxAge());
            /*
            string name = textBoxName.Text;
            string surname = textBoxSurname.Text;
            int age = Int32.Parse(textBoxAge.Text);
            */
            listOfPeople.Add(new Person(name, surname, age));
            dataGridViewListOfPeople.Refresh();
        }

        private void buttonAddGrade_Click(object sender, EventArgs e)
        {
            if (dataGridViewListOfPeople.SelectedRows.Count > 0)
            {
                formAddGrade = new FormAddGrade();
                formAddGrade.ShowDialog();
                var name = dataGridViewListOfPeople.SelectedRows[0].Cells[0].Value;
                listOfPeople.Find(person => person.Name == name).Grades.Add(new Grade(Int32.Parse(GetFormAddGradeTextBoxGrade())));
                dataGridViewGrades.DataSource = null;
                dataGridViewGrades.DataSource = listOfPeople.Find(person => person.Name == name).Grades;
            }
                else
            {
                MessageBox.Show("Nie zaznaczono osoby");
            }
        }

        private void buttonShowGrades_Click(object sender, EventArgs e)
        {
            dataGridViewGrades.Visible = true;
            labelMarks.Visible = true;
            pictureBoxSmile.Visible = true;
            pictureBoxSmile.Refresh();
            dataGridViewListOfPeople.DataSource = listOfPeople;

            if (dataGridViewListOfPeople.SelectedRows.Count>0)
            {
                var name = dataGridViewListOfPeople.SelectedRows[0].Cells[0].Value;
                dataGridViewGrades.DataSource = null;
                List<Grade> sourceGrades= listOfPeople.Find(person => person.Name == name).Grades;
                dataGridViewGrades.DataSource = sourceGrades;

                //jezeli dobra ocena to wyswietla obrazek
                int counterForBadMarks = 0;
                int counterForGoodMarks = 0;

            foreach( Grade grade in sourceGrades)
                {
                    if(grade.Value <= 2)
                    {
                        counterForBadMarks++;
                    }
                    if(grade.Value >= 3)
                    {
                        counterForGoodMarks++;
                    }
                }

                if (counterForBadMarks == 0 && counterForGoodMarks == 0 || counterForGoodMarks==counterForBadMarks)
                {
                    pictureBoxSmile.Image = KatarzynaBialasLab2.Properties.Resources.emoty1;
                }
                else
                {   
                    if (counterForGoodMarks > counterForBadMarks)
                    {
                        //happy
                        pictureBoxSmile.Image = KatarzynaBialasLab2.Properties.Resources.smile;
                    }
                    else
                    {
                        //sad
                        pictureBoxSmile.Image = KatarzynaBialasLab2.Properties.Resources.sad;
                    }
                }
            }
            else
            {
                MessageBox.Show("Nie zaznaczono osoby");
            }
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            //przypisanie nulla umowzliwa refresh
            dataGridViewListOfPeople.DataSource = null;
            dataGridViewListOfPeople.DataSource = listOfPeople;
        }

        #endregion
    }
}
