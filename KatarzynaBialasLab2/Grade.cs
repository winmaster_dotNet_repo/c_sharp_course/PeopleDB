﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatarzynaBialasLab2
{
    [Serializable]
    public class Grade 
    {
        public float Value { get; set; }

        public Grade( float value)
        {
            this.Value = value;
        }
    }
}
